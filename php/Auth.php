<?php
require_once realpath('../vendor/autoload.php');

use php\Controllers\User;

$connection = new User();

// Se apeleaza la inregistrare.
if (isset($_POST['AuthReg'])) {
    $data['email'] = $connection->checkDetails($_POST['email']);
    $prob_exist = $connection->verificare_email($data['email']);
    if ($prob_exist == 0) {
        $data['parola'] = $connection->checkDetails($_POST['password']);
        $data['nume_complet'] = $connection->checkDetails($_POST['fullname']);
        $data['adresa'] = $connection->checkDetails($_POST['address']);
        $data['sex'] = $connection->checkDetails($_POST['sex']);
        $data['telefon'] = $connection->checkDetails($_POST['phonenumber']);
        $data['nationalitate'] = $connection->checkDetails($_POST['Nationality']);
        $data['parola'] = password_hash($data['parola'], PASSWORD_BCRYPT);
        $succ = $connection->creare_membru($data);
    } else header("location:../index.php?&error=" . $prob_exist);
    if ($succ == 1) header("location:../index.php?error=1");
}

// Se apeleaza la autentificare.
if (isset($_POST['AuthLog'])) {
    $email = $connection->checkDetails($_POST['email']);
    $parola = $connection->checkDetails($_POST['password']);
    $succ = $connection->autentificare_membru($email, $parola);
    if ($succ == 1)
        if ($_SESSION['Tutorial'] == 1) header('location:../tutorial.php');
        else header('location:../profile.php');
    else if($succ == 0) header('location:../index.php?error=2');
    else if($succ == 2) header('location:../index.php?error=16');
    else if($succ == 3) header('location:../index.php?error=19');
}

// Se apeleaza la terminarea tutorialului.
if (isset($_POST['endtutorial'])) {
    $user = $_SESSION['ID_User'];
    $connection->actualizare_tutorial_status($user);
}

// Se apeleaza la adaugarea cardului personal.
if (isset($_POST['adauga_card_personal'])) {
    $data['numar_card'] = $connection->checkDetails($_POST['numar_card']);
    $data['numar_card'] = str_replace(' ', '', $data['numar_card']);
    $exista = $connection->verificare_card_existent($data['numar_card']);
    if ($exista == 0) {
        $data['sold_actual'] = $connection->checkDetails($_POST['sold_actual']);
        $data['moneda'] = $connection->checkDetails($_POST['moneda']);
        $data['swift'] = $connection->checkDetails($_POST['swift']);
        $data['cont_bancar'] = $connection->checkDetails($_POST['cont_bancar']);
        $data['data'] = $connection->checkDetails($_POST['data']).'-01';
        $data['cvv'] = $connection->checkDetails($_POST['cvv']);
        $succ = $connection->adauga_card_personal($data);
        if ($succ == 1) {
            // Adaugam cardul si in istoricul cardurilor.
            $id = $connection->get_card_id($data['numar_card']);
            if ($id > 0) {
                $connection->actualizare_istoric_card($id, $data['sold_actual']);
                header("location:../adauga_card.php?error=4");
            }
        } else header("location:../adauga_card.php?error=5");
    } else header("location:../adauga_card.php?error=6");
}

// Se apeleaza la adaugarea cardului pentru magazin.
if (isset($_POST['adauga_card_magazin'])) {
    $numar_card = $connection->checkDetails($_POST['numar_card']);
    $numar_card = str_replace(' ', '', $numar_card);
    $nume_magazin = $connection->checkDetails($_POST['nume_magazin']);
    $succ = $connection->adauga_card_magazin($nume_magazin, $numar_card);
    if ($succ == 1) header("location:../adauga_card.php?error=7");
    else header("location:../adauga_card.php?error=16");
}

// Se apeleaza la actualizarea parolei.
if (isset($_POST['actualizare_parola'])) {
    $pass = $connection->checkDetails($_POST['password']);
    $repass = $connection->checkDetails($_POST['repassword']);
    if ($pass === $repass) {
        $pass = password_hash($pass, PASSWORD_BCRYPT);
        $succ = $connection->actualizare_parola($_SESSION['ID_User'], $pass);
        if ($succ == 1) header("location:../profile.php?error=8");
        else header("location:../profile.php?error=9");
    }
}

// Se apeleaza la actulizarea profilului.
if (isset($_POST['actualizare_profil'])) {
    $nume = $connection->checkDetails($_POST['nume_complet']);
    $email = $connection->checkDetails($_POST['email']);
    $adresa = $connection->checkDetails($_POST['adresa']);
    $telefon = $connection->checkDetails($_POST['telefon']);
    $succ = $connection->actualizare_profil($_SESSION['ID_User'], $nume, $telefon, $email, $adresa);
    if ($succ == 1) header("location:../profile.php?error=10");
    else header("location:../profile.php?error=11");
}

// Se apeleaza la stergea unui card.
if (isset($_POST['sterge_card_personal'])) {
    $nr_card = $connection->checkDetails($_POST['numar_card']);
    $succ = $connection->stergere_card($nr_card,'personal');
    if ($succ == 1) header("location:../carduri_persoanele.php?error=12");
    else header("location:../carduri_persoanele.php?error=13");
}

// Se apeleaza la stergea unui card pentru magazine.
if (isset($_POST['sterge_card_magazin'])) {
    $nr_card = $connection->checkDetails($_POST['numar_card']);
    $succ = $connection->stergere_card($nr_card,'magazin');
    if ($succ == 1) header("location:../carduri_magazine.php?error=12");
    else header("location:../carduri_magazine.php?error=13");
}

if (isset($_POST['actualizare_card_personal'])) {
	$nr_card = $connection->checkDetails($_POST['numar_card']);
	$sold_actual = $connection->checkDetails($_POST['sold_actual']);
	$succ = $connection->actualizare_sold($nr_card,$sold_actual);
	if($succ == 1) header("location:../carduri_persoanele.php?error=20");
	else header("location:../carduri_persoanele.php?error=21");
}
?>
