<?php

namespace php\Controllers;

class User extends System
{
    public $data;

    public function __construct()
    {
        parent::__construct();
    }

    // Functie folosita la deconectare
    function deconectare()
    {
        unset($this->data);
        session_unset();
        session_destroy();
        header('location:index.php');
    }

    // Ia datele unui utilizator dupa id-ul primit ca parametru.
    public function get_data_from_current_user($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM accounts WHERE ID = ? ');
        $stmt->bindParam(1, $id);
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $date = [
                'nume' => $row['Nume'],
                'email' => $row['Email'],
                'adresa' => $row['Adresa'],
                'telefon' => $row['Telefon'],
                'nationalitate' => $row['Nationalitate'],
                'sex' => $row['Sex'],
            ];
        }
        return $date;
    }

    // Functie folosita la creare de conturi.
    public function creare_membru($data)
    {
        if ($data) {
            $stmt = $this->db->prepare("INSERT INTO accounts (Nume,Email,Password,Adresa,Telefon,Nationalitate,Tutorial,Sex) VALUES (?,?,?,?,?,?,?,?)");
            $stmt->bindValue(1, $data['nume_complet']);
            $stmt->bindValue(2, $data['email']);
            $stmt->bindValue(3, $data['parola']);
            $stmt->bindValue(4, $data['adresa']);
            $stmt->bindValue(5, $data['telefon']);
            $stmt->bindValue(6, $data['nationalitate']);
            $stmt->bindValue(7, 1);
            $stmt->bindValue(8, $data['sex']);
            $stmt->execute();
            return 1;
        } else return 0;
    }

    // Functie folosita la autentificare.
    public function autentificare_membru($usr, $pass)
    {
        $stmt = $this->db->prepare("SELECT * FROM accounts WHERE Email = ?");
        $stmt->bindParam(1, $usr);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                if (password_verify($pass, $row['Password'])) {
                    $ip = $this->gaseste_ip();
                    $result = $this->verifica_ip($ip, $row['ID']);
                    if ($result == 1) {
                        $_SESSION['ID_User'] = $row['ID'];
                        $_SESSION['Connected'] = 1;
                        $_SESSION['Tutorial'] = $row['Tutorial'];
                        return 1;
                    } else return 2;
                } else return 0;
            }
        } else return 3;
    }

    private function verifica_ip($ip, $id)
    {
        $stmt = $this->db->prepare("SELECT * FROM autentificari WHERE accountID = ? ");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $found = 0;
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                if (password_verify($ip, $row['IP'])) {
                    if ($row['Confirmat'] == 0) {
                        $this->trimite_email($id, $row['IP']);
                    }
                    if ($row['Confirmat'] == 1) return 1;
                }
            }
        }
        if ($stmt->rowCount() < 0 || $found == 0) {
            $ip = password_hash($ip, PASSWORD_BCRYPT);
            $this->adauga_dispozitiv($id, $ip);
            $this->trimite_email($id, $ip);
            return 0;
        }
    }

    private function adauga_dispozitiv($id, $ip)
    {
        $stmt = $this->db->prepare("INSERT INTO autentificari (accountID, IP, Confirmat) VALUES (?,?,?)");
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $ip);
        $stmt->bindValue(3, 0);
        $stmt->execute();
    }

    private function trimite_email($user_id, $ip) {
        $date = $this->get_data_from_current_user($user_id);
        $subject = '[Virtual Cards]E-mail-ul pentru confirmarea dispozitivului';
        $txt = '<h2>Buna ziua!</h2><br>';
        $txt.='<h5>V-am pregătit cu link pentru verificarea dispozitivilui. <a href="www.vespadev.ro/confirmare.php?confirm=' . $ip . '&id=' . $user_id . '">Click aici!</a></h5>';
        $headers = 'From: licenta@vespadev.ro' . '\r\n';
        $headers .= 'MIME-Version: 1.0' . '\r\n';
        $headers .= 'Content-type:text/html;charset=UTF-8' . '\r\n';
        mail($date['email'], $subject, $txt, $headers);
    }


    public function validare_dispozitiv($id, $ip)
    {
        $stmt = $this->db->prepare("UPDATE autentificari SET Confirmat = 1 WHERE accountID = ? AND IP = ? ");
        $stmt->bindParam(1, $id);
        $stmt->bindParam(2, $ip);
        $stmt->execute();
        if ($stmt->rowCount() > 0)
            header("location: index.php?error=17");
        else
            header("location: index.php?error=18");
    }

    // Functie folosita pentru a verifica daca exista deja un email.
    public function verificare_email($email)
    {
        $stmt = $this->db->prepare("SELECT * FROM accounts WHERE Email = ? ");
        $stmt->bindParam(1, $email);
        $stmt->execute();
        if ($stmt->rowCount() > 0) return 2;
        else return 0;
    }

    //Functie folosita la terminarea tutorialului.
    public function actualizare_tutorial_status($id)
    {
        $stmt = $this->db->prepare("UPDATE accounts SET Tutorial = 0 WHERE ID = ? ");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        header('location:../profile.php');
    }

    //Functie folosita la creare de card-uri personale.
    public function adauga_card_personal($data)
    {
        if ($data) {
            $stmt = $this->db->prepare("INSERT INTO carduri (accountID,NumarCard,Balanta,Moneda,SWIFT,Cont_Bancar,Expira) VALUES (?,?,?,?,?,?,?)");
            $stmt->bindValue(1, $_SESSION['ID_User']);
            $stmt->bindValue(2, $data['numar_card']);
            $stmt->bindValue(3, $data['sold_actual']);
            $stmt->bindValue(4, $data['moneda']);
            $stmt->bindValue(5, $data['swift']);
            $stmt->bindValue(6, $data['cont_bancar']);
            $stmt->bindValue(7, $data['data']);
            $stmt->execute();
            return 1;
        } else return 0;
    }

    //Functie folosita la creare de card-uri personale.
    public function adauga_card_magazin($nume, $numar)
    {
        if ($nume) {
            $stmt = $this->db->prepare("INSERT INTO card_cumparaturi (accountID,Nume,Numar) VALUES (?,?,?)");
            $stmt->bindValue(1, $_SESSION['ID_User']);
            $stmt->bindValue(2, $nume);
            $stmt->bindValue(3, $numar);
            $stmt->execute();
            return 1;
        } else return 0;
    }

    // Actualizare istoric.
    public function actualizare_istoric_card($id, $bani)
    {
        $stmt = $this->db->prepare("INSERT INTO istoric_card (cardID,Balanta) VALUES (?,?)");
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $bani);
        $stmt->execute();
        return 1;
    }

    // Functie ce intoarce id-ul cardului.
    public function get_card_id($numar_card)
    {
        $stmt = $this->db->prepare("SELECT ID FROM carduri WHERE NumarCard = ?");
        $stmt->bindValue(1, $numar_card);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) 
				$id = $row['ID'];
            return $id;
        } else return 0;
    }

    // Functie ce incarca cardurile utilizatorului conectat.
    public function date_carduri_magazine($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM card_cumparaturi WHERE accountID = ? ");
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $carduri = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $carduri[$row['ID']] = [
                'nume' => $row['Nume'],
                'numar' => $row['Numar']
            ];
        }
        return $carduri;
    }

    // Functie ce incarca cardurile personale utilizatorului conectat.
    public function date_carduri_personale($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM carduri AS card,accounts AS usr WHERE card.accountID = ? AND usr.ID = ? ");
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $id);
        $stmt->execute();
        $carduri = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data = date_create($row['Expira']);
            $carduri[$row['NumarCard']] = [
                'Nume' => $row['Nume'],
                'NumarCard' => $row['NumarCard'],
                'Balanta' => $row['Balanta'],
                'Moneda' => $row['Moneda'],
                'SWIFT' => $row['SWIFT'],
                'Cont_Bancar' => $row['Cont_Bancar'],
                'Expira' => date_format($data, "m/y"),
            ];
        }
        return $carduri;
    }

    // Functie ce verifica daca exista deja acest card in db.
    public function verificare_card_existent($numar_card)
    {
        $stmt = $this->db->prepare("SELECT * FROM carduri WHERE NumarCard = ? ");
        $stmt->bindValue(1, $numar_card);
        $stmt->execute();
        if ($stmt->rowCount() > 0) return 1;
        else return 0;
    }

    //Functie folosita la actualizarea parolei.
    public function actualizare_parola($id, $parola)
    {
        $stmt = $this->db->prepare("UPDATE accounts SET Password = ? WHERE ID = ? ");
        $stmt->bindValue(1, $parola);
        $stmt->bindValue(2, $id);
        $stmt->execute();
        if ($stmt->rowCount() > 0) return 1;
        else return 0;
    }

    // Functie folosita la actualizarea profilului.
    public function actualizare_profil($id, $nume, $telefon, $email, $adresa)
    {
        $stmt = $this->db->prepare("UPDATE accounts SET Nume = ? , Telefon = ? ,Email = ? , Adresa = ? WHERE ID = ? ");
        $stmt->bindValue(1, $nume);
        $stmt->bindValue(2, $telefon);
        $stmt->bindValue(3, $email);
        $stmt->bindValue(4, $adresa);
        $stmt->bindValue(5, $id);
        $stmt->execute();
        if ($stmt->rowCount() > 0) return 1;
        else return 0;
    }

    //Functie folosita la stergerea cardului dat.
    public function stergere_card($numar, $tip)
    {
        if ($tip === 'personal')
            $stmt = $this->db->prepare("DELETE FROM carduri WHERE NumarCard = ? ");
        else
            $stmt = $this->db->prepare("DELETE FROM card_cumparaturi WHERE Numar = ? ");
        $stmt->bindValue(1, $numar);
        $stmt->execute();
        if ($stmt->rowCount() > 0) return 1;
        else return 0;
    }

    public function date_schimb_valutar()
    {
        $stmt = $this->db->prepare("SELECT * FROM schimb_valutar ORDER BY Valoare Desc");
        $stmt->execute();
        $curs = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $curs[] = [
                'Denumire' => $row['Denumire'],
                'Moneda' => $row['Moneda'],
                'Valoare' => $row['Valoare'],
            ];
        }
        return $curs;
    }
	public function actualizare_sold($nr, $balanta) {
		$stmt = $this->db->prepare("UPDATE carduri SET Balanta = ? WHERE NumarCard = ? ");
        $stmt->bindValue(1, $balanta);
        $stmt->bindValue(2, $nr);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
			$id = $this->get_card_id($nr);
			$exec = $this->actualizare_istoric_card($id,$balanta);
			if ($exec == 1) return 1; else return 0;
		}
        else return 0;
	}
}

?>
