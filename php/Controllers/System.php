<?php

namespace php\Controllers;

class System
{
    public $db;
    private $folder = "subfiles", $componente;
    private $ip_baza_de_date,
        $nume_baza_de_date = "",
        $admin_baza_de_date = "",
        $parola_baza_de_date = "";

    // Contructorul la apelarea clasei User.
    function __construct()
    {
        // Construirea de componente
        if (is_dir($this->folder) == 1) $componente = scandir($this->folder);
        else $componente = scandir("../" . $this->folder);
        foreach ($componente as $componenta) {
            if (strlen($componenta) > 3) {
                $nume = substr_replace($componenta, "", -4);
                $this->componente[$nume] = $this->folder . "/" . $nume . ".php";
            }
        }
        // Seteaza ip-ul pentru conectarea la baza de date;
        if ($this->gaseste_ip() == '::1')
            $this->ip_baza_de_date = "45.13.252.103";

        else
            $this->ip_baza_de_date = "localhost";

        // Conexiunea la baza de date.
        if (session_status() == PHP_SESSION_NONE) session_start();
        try {
            $this->db = new \PDO('mysql:host=' . $this->ip_baza_de_date . ';dbname=' . $this->nume_baza_de_date . ';charset=utf8', $this->admin_baza_de_date, $this->parola_baza_de_date);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $this->db;
    }

    // Functie ce verifica textul primit ca parametru.
    function checkDetails($detail)
    {
        $detail = trim($detail);
        $detail = strip_tags($detail);
        $detail = htmlspecialchars($detail);
        return $detail;
    }

    // Verifica daca suntem autentificati altfel ne trimite sa ne autentificam.
    function verifica_conectat($id)
    {
        if (!isset($id)) {
            header("location:index.php");
        }
    }

    // Functie pentru returnarea erorii.
    function get_message_error($data)
    {
        if ($data) {
            $stmt = $this->db->prepare("SELECT * FROM coduri_eroare WHERE ID = ? ");
            $stmt->bindParam(1,$data);
            $stmt->execute();
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                return $row['Mesaj'];
            }
        }
    }

    public function afisare($tip, $nume)
    {
        switch ($tip) {
            case "form":
                require_once $this->afisare_form($nume);
                break;
            case "comp":
                require_once $this->componente($nume);
                break;
        }
    }

    private function afisare_form($tip)
    {
        switch ($tip) {
            case "autentificare":
                // Formular pentru autentificare
                return $this->componente['form_login'];
            case "inregistrare":
                // Formular pentru intregistrare
                return $this->componente['form_inreg'];
            case "parola":
                // Formular pentru resetare parola
                return $this->componente['form_parola'];
            case "profil":
                // Formular pentru actualizare profil
                return $this->componente['form_profil'];
            case "magazin":
                // Formular pentru adaugare carduri magazin
                return $this->componente['form_card_mag'];
            case "personal":
                // Formular pentru adaugare carduri personale
                return $this->componente['form_card_pers'];
        }
    }

    private function componente($tip)
    {
        switch ($tip) {
            case "meniu":
                // Componenta pentru afisarea meniului
                return $this->componente['meniu'];
            case "head":
                // Componenta pentru afisarea head-ului
                return $this->componente['head'];
            case "js":
                // Componenta pentru apelarea scriputilor js.
                return $this->componente['scripts'];
            case "magazin":
                // Componenta pentru afisarea cardurilor pentru magazin
                return $this->componente['card_mag'];
            case "personal":
                // Componenta pentru afisarea cardurilor personale
                return $this->componente['card_pers'];
            case "exchange":
                // Componenta pentru afisarea cursului valutar
                return $this->componente['schimb_valutar'];
            case "notificare":
                // Componenta pentru afisarea notificarii
                return $this->componente['notificare'];
            case "alerta":
                // Componenta pentru afisarea notificarii
                return $this->componente['alerta'];
        }
    }

    protected function gaseste_ip()
    {
        $servers = [
            "HTTP_CLIENT_IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_FORWARDED",
            "REMOTE_ADDR"
        ];
        foreach ($servers as $server) {
            if (isset($_SERVER[$server])) {
                $ip = $_SERVER[$server];
                break;
            }
        }
        return $ip;
    }
}
