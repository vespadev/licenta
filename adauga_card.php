<?php
require_once realpath('vendor/autoload.php');

use php\Controllers\User;

$user = new User();
$user->verifica_conectat($_SESSION['ID_User']);
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <?php $user->afisare("comp", "head"); ?>
</head>
<body>
<?php $user->afisare("comp", "meniu"); ?>
<div class="container-fluid">
    <div class="container container-fade">
        <div class="header">
            <h1>Adaugă card</h1>
            <a href="#" class="toggle-meniu"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>
        <?php
        $user->afisare("form", "magazin");
        $user->afisare("form", "personal");
        if (isset($_GET['error'])) $user->afisare("comp", "alerta");
        ?>
    </div>
</div>
<?php $user->afisare("comp", "js"); ?>
</body>
</html>
