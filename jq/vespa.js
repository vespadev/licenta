$(document).ready(function () {
    let formular_autentificare = ".sign-in-form",
        formular_inregistrare = ".sign-up-form",
        btn_inregistrare = "#sign-up",
        btn_autentificare = "#sign-in",
        btn_continuare = "#form-next",
        btn_inapoi = "#form-back",
        inregistrare_1 = ".first-step",
        inregistrare_2 = ".sec-step",
        clicked = false,
        numar = 0;


    $('.toggle-meniu').on("click", function () {
        $(".meniu").addClass("display-on");
        $("body").css("overflow", "hidden");
    });
    $('.ascunde-meniu').on("click", function () {
        $(".meniu").removeClass("display-on");
        $("body").css("overflow", "auto");
    });

    $(btn_inregistrare).on("click", function () {
        $("#sign-in-text").html("Înregistrare");
        switch_display(formular_autentificare, formular_inregistrare);
    });

    $(btn_autentificare).on("click", function () {
        $("#sign-in-text").html("Autentificare");
        switch_display(formular_inregistrare, formular_autentificare);
    });

    $(btn_inapoi).click(function () {
        switch_display(inregistrare_2, inregistrare_1);
    });

    $('input[name="numar_card"]').on("input", function () {
        var foo = $(this).val().split(" ").join("");
        if (foo.length > 0) {
            foo = foo.match(new RegExp('.{1,3}', 'g')).join(" ");
        }
        $(this).val(foo);
    });

	$('#numar_card').on("input", function () {
		let text = $(this).val().split(" ").join("");
		if (text.length > 0) text = text.match(new RegExp('.{1,4}', 'g')).join(" ");
		$(this).val(text);
	});

    $('input[name=adauga_card_magazin]').on('click', function (e) {
        let nr_char = $("input[name='numar_card']").val();
        if (nr_char.length < 15 || nr_char.length > 17) {
            e.preventDefault();
            if (clicked == false) {
                $("input[name='numar_card']").parent().append("<label>Codul nu este introdus corect!</label>");
                clicked = true;
            }
        }
    });

    $(btn_continuare).click(function () {
        let pass = $("#pass"),
            repass = $("#pass2");
        if (pass.val() == repass.val()) {
            switch_display(inregistrare_1, inregistrare_2);
        } else {
            pass.addClass("form-error");
            repass.addClass("form-error");
        }
    });

    $('input[name=actualizare_parola]').on('click', function (e) {
        let pass = $('input[name=password]'),
            repass = $('input[name=repassword]');
        if (pass.val() != repass.val()) {
            e.preventDefault();
            $('p.diff-pass').remove();
            $(this).parent().append('<p class="diff-pass">* câmpurile nu sunt identice</p>');
            pass.addClass("form-error");
            repass.addClass("form-error");
        } else if (pass.val().length < 6 ) {
            e.preventDefault();
            $('p.diff-pass').remove();
            $(this).parent().append('<p class="diff-pass">* parola este prea scurtă</p>');
            pass.addClass("form-error");
            repass.addClass("form-error");
        }
    });

    $('.card h3').on("click", function () {
        let numar_card = $(this).attr("numar");
        $(".overlay").css("display", "flex");
        $(".cod-de-bara").barcode(
            numar_card, // Value barcode (dependent on the type of barcode)
            "ean13" // type (string)
        );
    });

    $('.inchide').on("click", function () {
        $(".overlay").css("display", "none");
    });

    $('.apelare-confirmare').on("click", function (e) {
        return confirm("Sunteți sigur că doriți să ștergeți cardul?");
    });

    $('input.verificare-numar').on("change", function (e) {
        let text = $(this).val();
        text = text.replace(/\s/g, '');
        if (!$.isNumeric(text)) {
            $(this).addClass("form-error");
            numar = 1;
        } else {
            numar = 0;
            $(this).removeClass("form-error");
        }
    });

    $('input[type=submit]').on("click", function (e) {
        if (numar > 0) {
            e.preventDefault();
        }
    });

    $('#dtBasicExample').DataTable({
        "pagingType": "simple",
        "bFilter": false,
        "bInfo": false,
        "bLengthChange": false,
        "dom": 'ft<"paginatie"p>',
        "language": {
            "paginate": {
                "previous": "Înapoi",
                "next": "Următoarea"
            }
        }
    });
});

function switch_display($hide, $show) {
    $($show).show(500);
    $($hide).hide(500);
}