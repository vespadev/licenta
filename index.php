<?php
require_once realpath('vendor/autoload.php');

use php\Controllers\User;

$user = new User();

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <?php $user->afisare("comp", "head");?>
</head>
<body>
<?php if (isset($_GET['error'])) $user->afisare("comp", "alerta"); ?>
<div class="container-fluid">
    <div class="container container-fade">
        <div class="sign-up">
            <div class="top-sign-up">
                <h3 class=""><i class="fa fa-free-code-camp" aria-hidden="true"></i></h3>
                <h4 id="sign-in-text">Autentificare</h4>
            </div>
            <?php
            $user->afisare("form", "autentificare");
            $user->afisare("form", "inregistrare");
            ?>
        </div>
    </div>
</div>
<?php $user->afisare("comp", "js");?>
</body>
</html>
