<?php
require_once realpath('vendor/autoload.php');

use php\Controllers\User;

$user = new User();
$user->verifica_conectat($_SESSION['ID_User']);
if ($_SESSION['Tutorial'] == 0) header('location:profile.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <?php $user->afisare('comp', 'head'); ?>
</head>
<body>
<div class="container-fluid">
    <div class="container container-fade tutorial-height">
        <div class="top-sign-up tutorial-top">
            <h3 class=""><i class="fa fa-free-code-camp" aria-hidden="true"></i></h3>
        </div>
        <div class="taburi-tutorial">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#profil">1. Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#card-mag">2. Carduri fidelitate</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#card-pers">3. Carduri bancare</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#adauga-card">4. Adăugare carduri</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#schimb-valutar">5. Schimb valutar</a>
                </li>
                <li class="nav-item">
                    <form action="php/Auth.php" method="post">
                        <input type="submit" class="nav-link" name="endtutorial" value="Deschide aplicația">
                    </form>
                </li>
            </ul>
        </div>
        <div class="tab-content pagina-tutorial">
            <div id="profil" class="container tab-pane active"><br>
                <h3><strong>1.</strong>Cum îmi actualizez profilul?</h3>
                <div class="row">
                    <div class="col-md-8">
                        <img src="img/tutorial/actualizare_profil.png" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <p>În stânga v-am pregătit o poză pentru a ști cum arată formularul de actualizare a profilului.</p>
                        <p>Acest formular este disponibil oricărui utilizator după autentificare.</p>
                    </div>
                </div>
                <h3><strong>2.</strong>Cum îmi schimb parola?</h3>
                <div class="row">
                    <div class="col-md-4">
                        <p>În dreapta v-am pregătit o poză pentru a ști cum arata formularul de actualizare a parolei.</p>
                        <p>Pentru o singuranță mai mare recomandăm schimbarea parolei cât mai des posibil.</p>
                    </div>
                    <div class="col-md-8">
                        <img src="img/tutorial/actualizare_parola.png" class="img-responsive">
                    </div>
                </div>
            </div>
            <div id="card-mag" class="container tab-pane fade"><br>
                <h3><strong>1.</strong>Cum îmi dau seama dacă am sau nu vreun card adăugat?</h3>
                <div class="row">
                    <div class="col-md-8">
                        <img src="img/tutorial/no_card_mag.png" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <p>Următoarea alertă va apărea dacă nu veți avea carduri de fidelitate adăugate pe cont.</p>
                    </div>
                </div>
                <h3><strong>2.</strong>Cum va fi afișată listarea cardurilor?</h3>
                <div class="row">
                    <div class="col-md-4">
                        <p>Listarea cardurilor va fi afișată astfel</p>
                    </div>
                    <div class="col-md-8">
                        <img src="img/tutorial/list_card_mag.png" class="img-responsive">
                    </div>
                </div>
                <h3><strong>3.</strong>Cum va fi afișat un card?</h3>
                <div class="row">
                    <div class="col-md-8">
                        <img src="img/tutorial/barcode.png" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <p>Cardurile din aplicație au fost testate in magazine precum Penny și Profi</p>
                    </div>
                </div>
            </div>
            <div id="card-pers" class="container tab-pane fade"><br>
                <h3><strong>1.</strong>Cum îmi dau seama dacă am sau nu vreun card adăugat?</h3>
                <div class="row">
                    <div class="col-md-8">
                        <img src="img/tutorial/no_card_pers.png" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <p>Următoarea alertă va apărea dacă nu veți avea carduri de credit adăugate pe cont.</p>
                    </div>
                </div>
                <h3><strong>2.</strong>Cum va fi afișată listarea cardurilor?</h3>
                <div class="row">
                    <div class="col-md-4">
                        <p>Listarea cardurilor va fi afișată astfel:</p>
                    </div>
                    <div class="col-md-8">
                        <img src="img/tutorial/list_card_pers.png" class="img-responsive">
                    </div>
                </div>
            </div>
            <div id="adauga-card" class="container tab-pane fade"><br>
                <h3><strong>1.</strong> Cum adaug un card de fidelitate?</h3>
                <div class="row">
                    <div class="col-md-8">
                        <img src="img/tutorial/add_card_mag.png" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <p>Acest formular este format din două câmpuri:<br>
                            1. <strong>Nume magazin</strong> - va fi folosit pentru etichetarea cardului<br>
                            2. <strong>Numar card</strong> - codul de pe cardul dumneavoastră.
                        </p>
                    </div>
                </div>
                <h3><strong>2.</strong>Cum adaug un card de credit?</h3>
                <div class="row">
                    <div class="col-md-4">
                        <p>Formularul de adăugare:</p>
                    </div>
                    <div class="col-md-8">
                        <img src="img/tutorial/add_card_pers.png" class="img-responsive">
                    </div>
                </div>
            </div>
            <div id="schimb-valutar" class="container tab-pane fade"><br>
                <h3><strong>1.</strong> Curs valutar</h3>
                <div class="row">
                    <div class="col-md-4">
                        <p>Cursul valutar este sincronizat cu cel de pe cursbnr.ro</p>
                    </div>
                    <div class="col-md-8">
                        <img src="img/tutorial/curs_valutar.png" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $user->afisare('comp', 'js'); ?>
</body>
</html>
