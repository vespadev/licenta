-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 24, 2020 at 11:45 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u233089348_licenta`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `ID` int(15) NOT NULL,
  `Nume` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` text NOT NULL,
  `Adresa` varchar(255) NOT NULL,
  `Telefon` varchar(255) NOT NULL,
  `Nationalitate` varchar(50) NOT NULL,
  `Tutorial` int(1) NOT NULL,
  `Sex` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `autentificari`
--

CREATE TABLE `autentificari` (
  `ID` int(100) NOT NULL,
  `accountID` int(100) NOT NULL,
  `IP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Confirmat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `autentificari`
--


-- --------------------------------------------------------

--
-- Table structure for table `carduri`
--

CREATE TABLE `carduri` (
  `ID` int(60) NOT NULL,
  `accountID` int(50) NOT NULL,
  `NumarCard` varchar(16) NOT NULL,
  `Balanta` float NOT NULL,
  `Moneda` varchar(5) NOT NULL,
  `SWIFT` varchar(11) NOT NULL,
  `Cont_Bancar` varchar(255) NOT NULL,
  `Expira` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carduri`
--

-- --------------------------------------------------------

--
-- Table structure for table `card_cumparaturi`
--

CREATE TABLE `card_cumparaturi` (
  `ID` int(50) NOT NULL,
  `accountID` int(50) NOT NULL,
  `Nume` varchar(255) NOT NULL,
  `Numar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `card_cumparaturi`
--

-- --------------------------------------------------------

--
-- Table structure for table `coduri_eroare`
--

CREATE TABLE `coduri_eroare` (
  `ID` int(100) NOT NULL,
  `Mesaj` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coduri_eroare`
--

INSERT INTO `coduri_eroare` (`ID`, `Mesaj`) VALUES
(1, 'Contul dumneavoastră a fost creat cu success!'),
(2, 'Datele de autentificare au fost introduse greșit.'),
(3, 'E-mail-ul dumneavoastră este deja folosit pe această platformă.'),
(4, 'Cardul dumneavoastră a fost adăugat cu success!'),
(5, 'Datele introduse nu sunt corecte!'),
(6, 'Cardul introdus există deja în sistemul nostru!'),
(7, 'Cardul de cumparaturi a fost adăugat cu success!'),
(8, 'Parola dumneavoastră a fost actualizată!'),
(9, 'Parola nu a putut fi actualizată!'),
(10, 'Profilul dumneavoastră a fost actualizat.'),
(11, 'Profilul dumneavoastră nu a fost actualizat, vă rugăm reîncercați.'),
(12, 'Cardul a fost șters cu success!'),
(13, 'Ștergerea cardului a întampinat o problemă!'),
(14, 'Nu aveți nici un card introdus!'),
(15, 'Cardul de cumpărături nu a fost adăugat, vă rugăm să încercați din nou.'),
(16, 'A fost detectat un nou dispozitiv, vă rugăm să accesați e-mail-ul pentru a-l confirma.'),
(17, 'Dispozitivul dumneavoastră a fost verificat cu succes!'),
(18, 'Dispozitivul nu a fost verificat cu succes, va rugăm să încercați din nou.'),
(19, 'E-mail-ul dumneavoastră nu este înregistrat pe această platformă.'),
(20, 'Balanța dumneavoastră a fost actualizată.'),
(21, 'Sistemul a întâmpinat o problemă, vă rugăm încercați mai târziu.');

-- --------------------------------------------------------

--
-- Table structure for table `istoric_card`
--

CREATE TABLE `istoric_card` (
  `ID` int(100) NOT NULL,
  `cardID` int(100) NOT NULL,
  `Balanta` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------

--
-- Table structure for table `schimb_valutar`
--

CREATE TABLE `schimb_valutar` (
  `ID` int(100) NOT NULL,
  `Denumire` varchar(255) NOT NULL,
  `Moneda` varchar(255) NOT NULL,
  `Valoare` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `schimb_valutar`
--

INSERT INTO `schimb_valutar` (`ID`, `Denumire`, `Moneda`, `Valoare`) VALUES
(2, 'Dolarul SUA', 'USD', 4.286),
(3, 'Francul elveţian', 'CHF', 4.5355),
(4, 'Lira sterlină', 'GBP', 5.3611),
(5, 'Leva bulgarească', 'BGN', 2.4763),
(6, 'Rubla rusească', 'RUB', 0.062),
(7, 'Randul sud-african', 'ZAR', 0.2478),
(8, 'Realul brazilian', 'BRL', 0.8319),
(9, 'Renminbi-ul chinezesc', 'CNY', 0.606),
(10, 'Rupia indiană', 'INR', 0.0566),
(11, '100 Woni sud-coreeni', '100KRW', 0.3565),
(12, 'Peso-ul mexican', 'MXN', 0.1906),
(13, 'Dolarul neo-zeelandez', 'NZD', 2.7575),
(14, 'Dinarul sârbesc', 'RSD', 0.0412),
(15, 'Hryvna ucraineană', 'UAH', 0.1608),
(16, 'DST', 'XDR', 5.9257),
(17, 'Noua lira turcească', 'TRY', 0.6249),
(18, 'Gramul de aur', 'XAU', 245.054),
(19, 'Dolarul australian', 'AUD', 2.9637),
(20, 'Dolarul canadian', 'CAD', 3.1592),
(21, 'Coroana cehă', 'CZK', 0.1815),
(22, 'Coroana daneză', 'DKK', 0.6499),
(23, 'Lira egipteană', 'EGP', 0.2651),
(24, '100 Forinți maghiari', '100HUF', 1.3788),
(25, '100 Yeni japonezi', '100JPY', 4.0233),
(26, 'Leul moldovenesc', 'MDL', 0.247),
(27, 'Coroana norvegiană', 'NOK', 0.4483),
(28, 'Zlotul polonez', 'PLN', 1.0881),
(29, 'Coroana suedeză', 'SEK', 0.46),
(30, 'Dirhamul Emiratelor', 'AED', 1.167),
(31, 'Kuna croată', 'HRK', 0.6392),
(32, 'Bahtul thailandez', 'THB', 0.139),
(33, 'Euro', 'EUR', 4.8432);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `autentificari`
--
ALTER TABLE `autentificari`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `carduri`
--
ALTER TABLE `carduri`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `card_cumparaturi`
--
ALTER TABLE `card_cumparaturi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `coduri_eroare`
--
ALTER TABLE `coduri_eroare`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `istoric_card`
--
ALTER TABLE `istoric_card`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `schimb_valutar`
--
ALTER TABLE `schimb_valutar`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `autentificari`
--
ALTER TABLE `autentificari`
  MODIFY `ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `carduri`
--
ALTER TABLE `carduri`
  MODIFY `ID` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `card_cumparaturi`
--
ALTER TABLE `card_cumparaturi`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `coduri_eroare`
--
ALTER TABLE `coduri_eroare`
  MODIFY `ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `istoric_card`
--
ALTER TABLE `istoric_card`
  MODIFY `ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `schimb_valutar`
--
ALTER TABLE `schimb_valutar`
  MODIFY `ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
