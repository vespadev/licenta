<div class="meniu">
    <ul>
        <li><button class="fa fa-times-circle-o ascunde-meniu"></button></li>
        <li><a href="profile.php"><i class="fa fa-user-o" aria-hidden="true"></i> Profil</a></li>
        <li><a href="carduri_magazine.php"><i class="fa fa-money" aria-hidden="true"></i> Carduri fidelitate</a></li>
        <li><a href="carduri_persoanele.php"><i class="fa fa-credit-card" aria-hidden="true"></i> Carduri bancare</a>
        </li>
        <li><a href="adauga_card.php"><i class="fa fa-credit-card" aria-hidden="true"></i> Adaugă carduri</a></li>
        <li><a href="schimb_valutar.php"><i class="fa fa-exchange" aria-hidden="true"></i> Schimb valutar</a></li>
        <li><a href="signout.php"><i class="fa fa-sign-out" aria-hidden="true"></i></i> Deconectare</a></li>
    </ul>
</div>