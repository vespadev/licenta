<div class="schimb-valutar">

    <ul><h3>Informații</h3>
        <li><p>* datele cursului valutar sunt preluate de pe <strong>cursbnr.ro</strong></p></li>
        <li><p>* datele cursului valutar se actualizează automat la fiecare 5 minute</p></li>
        <li><p>* tabelul poate fi sortat dupa (Nume, Moneda, Curs) printr-un singur click</p></li>
    </ul>
    <div class="date-schimb">
        <table class="table table-dark table-hover table-striped" id="dtBasicExample">
            <thead>
            <tr>
                <th>Nume</th>
                <th>Monedă</th>
                <th>Curs</th>
            </tr>
            </thead>
            <tbody>
            <?php
                use php\Controllers\User;
                $user = new User;
                $curs = $user->date_schimb_valutar();
                foreach ($curs as $element) {
            ?>
            <tr>
                <td><?= $element['Denumire'];?></td>
                <td><?= $element['Moneda'];?></td>
                <td><?= $element['Valoare'];?></td>
            </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>