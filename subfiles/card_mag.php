<?php

use php\Controllers\User;

?>
<div class="carduri-magazin">
    <?php
	$user = new User();
	$carduri = $user->date_carduri_magazine($_SESSION['ID_User']);
	if (!$carduri) $user->afisare("comp", "notificare");
    else
        foreach ($carduri as $card) {
            ?>
            <div class="card">
                <h3 numar="<?= $card['numar']; ?>"><?= $card['nume']; ?></h3>
                <div class="sterge-card-magazin">
                    <form method="post" action="php/Auth.php">
                        <input type="hidden" name="numar_card" value="<?= $card['numar']; ?>">
                        <button class="fa fa-times-circle apelare-confirmare" aria-hidden="true"
                                name="sterge_card_magazin"></button>
                    </form>
                </div>
            </div>
            <?php } ?>
</div>
<div class="overlay">
    <i class="fa fa-times inchide" aria-hidden="true"></i>
    <div class="cod-de-bara"></div>
</div>