<form method="post" action="php/Auth.php">
    <div class="date-profil">
        <h3>Actualizare parolă</h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="pass">Parolă:</label>
                    <input type="password" class="form-control" id="pass" name="password" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="pass2">Reintroduceți parolă:</label>
                    <input type="password" class="form-control" id="pass2" name="repassword" required>
                </div>
            </div>
        </div>
        <input type="submit" name="actualizare_parola" value="Actualizare" class="btn btn-primary sing-in">
    </div>
</form>
<?php

use php\Controllers\System;

$system = new System();
if (isset($_GET['error'])) $system->afisare("comp", "alerta");
?>
