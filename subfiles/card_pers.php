<?php

use php\Controllers\User;
?>
<div class="carduri-personale">
    <?php
	$user = new User();
	$data = $user->date_carduri_personale($_SESSION['ID_User']);
	if (!$data) $user->afisare("comp", "notificare");
    else foreach ($data as $card) {
        ?>
        <form action="php/Auth.php" method="post">
            <div class="optiuni-card">
                <div class="card">
                    <h4><?= $card['Nume']; ?></h4>
                    <h4><?= $card['NumarCard']; ?></h4>
                    <h4><?= $card['Cont_Bancar']; ?></h4>
                    <h4><?= $card['SWIFT'] ?></h4>
                    <div class="date-subsol">
                        <h4>EXP<p><?= $card['Expira']; ?></p></h4>
                        <h4>BALANȚĂ<p><input typ="number" name="sold_actual" value="<?= $card['Balanta']; ?>" class="verificare-numar"></p></h4>
                        <h4>MONEDĂ<p><?= $card['Moneda']; ?></p></h4>
                    </div>
                </div>
                <div class="optiuni">

                    <input type="hidden" name="numar_card" value="<?= $card['NumarCard']; ?>">
                    <input type="submit" name="sterge_card_personal" value="Șterge card"
                           class="btn btn-primary sing-in apelare-confirmare">
                    <input type="submit" name="actualizare_card_personal" value="Actualizare sold"
                           class="btn btn-primary sing-in">

                </div>
            </div>
        </form>
        <?php
    }
    ?>
</div>
