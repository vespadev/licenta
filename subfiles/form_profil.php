<?php

use php\Controllers\User;

$user = new User();
$date = $user->get_data_from_current_user($_SESSION['ID_User']);
if (isset($_GET['error'])) $user->afisare("comp", "alerta");
?>
<form method="post" action="php/Auth.php" class="actualizare-profil">
    <div class="date-profil">
        <h3>Profil utilizator</h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nume complet</label>
                    <input type="text" name="nume_complet" class="form-control"
                           value="<?= $date['nume']; ?>" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Naționalitate</label>
                    <input type="text" name="nationalitate" class="form-control"
                           value="<?= $date['nationalitate']; ?>" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Adresă</label>
                    <input type="text" name="adresa" class="form-control" value="<?= $date['adresa']; ?>"
                           required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Telefon</label>
                    <input type="text" name="telefon" class="form-control verificare-numar"
                           value="<?= $date['telefon']; ?>"
                           required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="<?= $date['email']; ?>"
                           required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="sex">Sex</label>
                    <input type="text" name="sex" class="form-control" value="<?= $date['sex']; ?>"
                           disabled>
                </div>
            </div>
        </div>
        <input type="submit" name="actualizare_profil" value="Actualizare" class="btn btn-primary sing-in">
    </div>
</form>