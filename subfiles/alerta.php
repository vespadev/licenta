<?php
use php\Controllers\System;
$system = new System();
?>
<div class="alerta">
    <div class="info-alert">
        <img src="img/alert.svg" class="img-responsive">
        <h3><?= $system->get_message_error($_GET['error']);?></h3>
    </div>
</div>