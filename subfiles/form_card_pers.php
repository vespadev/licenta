<form method="post" action="php/Auth.php">
    <div class="date-profil">
        <h3>Adaugă card bancar</h3>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Număr card</label>
                    <input type="text" id="numar_card" name="numar_card" class="form-control verificare-numar"
                           placeholder="1234 4567 6655 3234" required maxlength="19">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Sold actual</label>
                    <input type="number" name="sold_actual" class="form-control verificare-numar" placeholder="100"
                           required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Monedă</label>
                    <select name="moneda" class="form-control">
                        <option value="RON">RON</option>
                        <option value="EUR">EUR</option>
                        <option value="USD">USD</option>
                        <option value="GBP">GBP</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>SWIFT / BIC</label>
                    <input type="text" name="swift" class="form-control" placeholder="BTRLRO22" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Cont bancar</label>
                    <input type="text" name="cont_bancar" class="form-control" placeholder="RO98BTRLEURCRT0200******"
                           required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Data expirării:</label>
                    <input type="month" name="data" class="form-control" required">
                </div>
            </div>
        </div>
        <input type="submit" name="adauga_card_personal" value="Adaugă card" class="btn btn-primary sing-in">
    </div>
</form>