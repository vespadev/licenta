<div class="sign-up-form">
    <form action="php/Auth.php" method="post">
        <div class="first-step">
            <div class="form-group">
                <label for="email">Adresă de e-mail:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="pass">Parolă:</label>
                <input type="password" class="form-control" id="pass" name="password">
            </div>
            <div class="form-group">
                <label for="pass2">Reintrodu parolă:</label>
                <input type="password" class="form-control" id="pass2" name="repassword">
            </div>
            <div class="form-group">
                <h4 class="text-continue">
                    <a class="btn btn-primary sing-in" id="form-next">Continuă</a>
                </h4>
            </div>
        </div>
        <div class="sec-step">
            <div class="form-group">
                <label for="email">Nume complet:</label>
                <input type="text" class="form-control" id="email" name="fullname"
                       placeholder="Viespe George">
            </div>
            <div class="form-group">
                <label for="adresa">Adresă:</label>
                <input type="text" class="form-control" id="adresa" name="address"
                       placeholder="Str New Balance,Bl.2,Et.3,Ap.33">
            </div>
            <div class="form-group">
                <label for="telefon">Telefon:</label>
                <input type="text" class="form-control verificare-numar" id="telefon" name="phonenumber"
                       placeholder="0773353696">
            </div>
            <div class="form-group">
                <label for="nationalitate">Naționalitate:</label>
                <input type="text" class="form-control" id="nationalitate" name="Nationality" placeholder="România">
            </div>
            <div class="form-group">
                <label for="sex">Sex:</label>
                <select class="form-control custom-select-md" id="sex" name="sex">
                    <option value="Masculin">Masculin</option>
                    <option value="Feminin">Feminin</option>
                </select>
            </div>
            <div class="form-group">
                <h4 class="finish-register">
                    <a class="btn btn-primary sing-in" id="form-back">Înapoi</a>
                    <button type="submit" class="btn btn-primary sing-in" name="AuthReg">Confirmă</button>
                </h4>
            </div>
        </div>
        <h5>Ai cont? <label><a href="#" id="sign-in"> Autentifică-te</a></label></h5>
    </form>
</div>
