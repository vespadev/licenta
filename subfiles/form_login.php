<form action="php/Auth.php" method="post" class="sign-in-form">
    <div class="form-group">
        <label for="email">Adresă de e-mail:</label>
        <input type="email" class="form-control" id="email" name="email">
    </div>
    <div class="form-group">
        <label for="pwd">Parolă:</label>
        <input type="password" class="form-control" id="pwd" name="password">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary sing-in" name="AuthLog">Autentificare</button>
    </div>
    <h5>Nu ai cont creat? <label><a href="#" id="sign-up">Creează-ți acum</a></label></h5>
</form>