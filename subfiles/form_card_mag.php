<form method="post" action="php/Auth.php" class="actualizare-card-magazin">
    <div class="date-profil">
        <h3>Adaugă card de fidelitate</h3>
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label>Nume magazin</label>
                    <input type="text" name="nume_magazin" class="form-control" placeholder="Farmacie, Supermarket, etc"
                           required>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="form-group">
                    <label>Număr card</label>
                    <input type="text" name="numar_card" class="form-control verificare-numar"
                           placeholder="123 467 665 388" required>
                </div>
            </div>
        </div>
        <input type="submit" name="adauga_card_magazin" value="Adaugă card" class="btn btn-primary sing-in">
    </div>
</form>